package com.gamedeveloping.shadowmaster.fontstylelibrary.fontutils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ShadowMaster on 25.06.2017.
 */

public class FontUtils {

    private final static String TAG = FontUtils.class.getName();

    private HashMap<String, TypefaceFamily> typeFaceMap = new HashMap<>();
    private HashMap<String, FamilyConfiguration> familyMap = new HashMap<>();

    private static FontUtils fontUtils;

    private FontUtils() {}

    private Map<String, FamilyConfiguration> getFamilyMap() {

        return familyMap;

    }

    public void enableLog() {

        Logger.enable();

    }

    public void disableLog() {

        Logger.disable();

    }

    public static FontUtils getInstance() {

        if (fontUtils == null) {

            return new FontUtils();

        } else {

            return fontUtils;

        }

    }

    public void registerFamily(String family, FamilyConfiguration configuration) {

        if (!familyMap.containsKey(family)) {

            familyMap.put(family, configuration);

        }

    }

    public Typeface loadTypeface(Context context, String family, FontStyle type) {

        if (type == null || !familyMap.containsKey(family)) return null;

        if (!typeFaceMap.containsKey(family)) {

            TypefaceFamily typefaceFamily = new TypefaceFamily(family);
            typeFaceMap.put(family, typefaceFamily);

        }

        return loadTypeface(context, family, type);

    }

    private Typeface loadTypeface(Context context, TypefaceFamily typefaceFamily, FontStyle type) {

        if (typefaceFamily.getTypeface(type) != null) {

            return typefaceFamily.getTypeface(type);

        } else {

            Typeface typeface = createTypeface(context, PathCreator.createPossiblePaths(
                    familyMap.get(typefaceFamily.getName()), type));
            typefaceFamily.setTypeface(typeface, type);
            return typeface;

        }

    }

    private Typeface createTypeface(Context context, List<String> possiblePaths) {

        for (String path : possiblePaths) {

            try {

                return Typeface.createFromAsset(context.getAssets(), path);

            } catch (RuntimeException e) {


            }

        }

        for (String path : possiblePaths) {

            Logger.v(TAG, "Fail loading typeface " + path);

        }

        return null;

    }

}
