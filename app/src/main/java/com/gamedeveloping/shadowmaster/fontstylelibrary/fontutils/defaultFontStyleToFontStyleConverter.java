package com.gamedeveloping.shadowmaster.fontstylelibrary.fontutils;

import android.graphics.Typeface;

/**
 * Created by ShadowMaster on 26.06.2017.
 */

public class defaultFontStyleToFontStyleConverter implements Converter<Integer, FontStyle>{


    @Override
    public FontStyle forward(Integer fromObject) {

        switch (fromObject) {

            case Typeface.NORMAL:
                return FontStyle.REGULAR;

            case Typeface.BOLD:
                return FontStyle.BOLD;

            case Typeface.ITALIC:
                return FontStyle.ITALIC;

            case Typeface.BOLD_ITALIC:
                return FontStyle.BOLD_ITALIC;

            default:
                return FontStyle.UNKNOWN;

        }

    }

    @Override
    public Integer backward(FontStyle fromObject) {

        switch (fromObject) {

            case REGULAR:
                return Typeface.NORMAL;

            case BOLD:
                return Typeface.BOLD;

            case ITALIC:
                return Typeface.ITALIC;

            case BOLD_ITALIC:
                return Typeface.BOLD_ITALIC;

            default:
                return null;

        }

    }

}
