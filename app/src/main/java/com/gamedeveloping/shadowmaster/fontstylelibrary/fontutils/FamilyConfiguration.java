package com.gamedeveloping.shadowmaster.fontstylelibrary.fontutils;

/**
 * Created by ShadowMaster on 29.07.2017.
 */

public class FamilyConfiguration {

    private final String path;
    private final String divider;
    private final String[] extensions;
    private final String family;

    public FamilyConfiguration(String family, String path, String divider, String[] extensions) {

        this.path = path;
        this.divider = divider;
        this.family = family;

        if (extensions != null) {

            this.extensions = extensions;

        } else {

            this.extensions = new String[]{".otf", ".ttf"};

        }

    }

    public String getPath() {
        return path;
    }

    public String getDivider() {
        return divider;
    }

    public String[] getExtensions() {
        return extensions;
    }

    public String getFamily() {
        return family;
    }
}
