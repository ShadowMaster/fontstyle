package com.gamedeveloping.shadowmaster.fontstylelibrary.fontutils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ShadowMaster on 29.07.2017.
 */

class PathCreator {

    static List<String> createPossiblePaths(FamilyConfiguration configuration, FontStyle style) {

        ArrayList<String> result = new ArrayList<>();

        for (String extension : configuration.getExtensions()) {

            result.add(configuration.getPath() + configuration.getFamily() + configuration.getDivider() + style.getCode() + extension);

        }

        return result;

    }

}
