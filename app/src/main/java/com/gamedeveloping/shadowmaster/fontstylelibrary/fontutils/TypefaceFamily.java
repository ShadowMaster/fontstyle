package com.gamedeveloping.shadowmaster.fontstylelibrary.fontutils;

import android.graphics.Typeface;

import java.util.HashMap;

/**
 * Created by ShadowMaster on 25.06.2017.
 */

class TypefaceFamily {

    private HashMap<FontStyle, Typeface> family = new HashMap<>();
    private  String name;

    TypefaceFamily(String name) {

        this.name = name;

    }

    String getName() {

        return name;

    }

    TypefaceFamily setTypeface(Typeface typeface, FontStyle type) {

        family.put(type, typeface);

        return this;

    }

    Typeface getTypeface(FontStyle type) {

       return family.get(type);

    }


}
