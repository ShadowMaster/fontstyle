package com.gamedeveloping.shadowmaster.fontstylelibrary.fontutils;

/**
 * Created by ShadowMaster on 29.07.2017.
 */

interface Converter <From, To>{

    To forward(From from);
    From backward(To to);

}
