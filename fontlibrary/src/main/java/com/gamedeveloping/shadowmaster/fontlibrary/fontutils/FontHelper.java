package com.gamedeveloping.shadowmaster.fontlibrary.fontutils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ShadowMaster on 25.06.2017.
 */

public class FontHelper {

    private final static String TAG = FontHelper.class.getName();

    private HashMap<String, TypefaceFamily> typeFaceMap = new HashMap<>();
    private HashMap<String, FamilyConfiguration> familyMap = new HashMap<>();

    private static FontHelper fontHelper;

    private FontHelper() {}

    private Map<String, FamilyConfiguration> getFamilyMap() {

        return familyMap;

    }

    public void enableLog() {

        Logger.enable();

    }

    public void disableLog() {

        Logger.disable();

    }

    public static FontHelper getInstance() {

        if (fontHelper == null) {

            return fontHelper = new FontHelper();

        } else {

            return fontHelper;

        }

    }

    public void registerFamily(FamilyConfiguration configuration) {

        if (!familyMap.containsKey(configuration.getFamily())) {

            familyMap.put(configuration.getFamily(), configuration);

        }

    }

    public Typeface loadTypeface(Context context, String family, FontStyle type) {

        if (type == null || !familyMap.containsKey(family)) return null;

        TypefaceFamily typefaceFamily;

        if (!typeFaceMap.containsKey(family)) {

            typefaceFamily = new TypefaceFamily(family);
            typeFaceMap.put(family, typefaceFamily);

        } else {

            typefaceFamily = typeFaceMap.get(family);

        }

        return loadTypeface(context, typefaceFamily, type);

    }

    private Typeface loadTypeface(Context context, TypefaceFamily typefaceFamily, FontStyle type) {

        if (typefaceFamily.getTypeface(type) != null) {

            return typefaceFamily.getTypeface(type);

        } else {

            Typeface typeface = createTypeface(context, PathCreator.createPossiblePaths(
                    familyMap.get(typefaceFamily.getName()), type));
            typefaceFamily.setTypeface(typeface, type);
            return typeface;

        }

    }

    private Typeface createTypeface(Context context, List<String> possiblePaths) {

        for (String path : possiblePaths) {

            try {

                return Typeface.createFromAsset(context.getAssets(), path);

            } catch (RuntimeException e) {


            }

        }

        for (String path : possiblePaths) {

            Logger.v(TAG, "Fail loading typeface " + path);

        }

        return null;

    }

}
