package com.gamedeveloping.shadowmaster.fontlibrary.fontutils;

/**
 * Created by ShadowMaster on 26.06.2017.
 */

public enum FontStyle {

    UNKNOWN("Unknown", 0), BOLD("Bold", 1), ITALIC("Italic", 2), BOLD_ITALIC("BoldItalic", 3),
    REGULAR("Regular", 4), MEDIUM("Medium", 5), LIGHT("Light", 6), OBLIQUE("Oblique", 7),
    BLACK("Black", 8), BOOK("Book", 9), MEDIUM_ITALIC("MediumItalic", 10),
    BOLD_OBLIQUE("BoldOblique", 11), LIGHT_ITALIC("LightItalic", 12),
    EXTRA_BOLD("ExtraBold", 13), BLACK_ITALIC("BlackItalic", 14), BOOK_ITALIC("BookItalic", 15),
    SEMI_BOLD("SemiBold", 16), SEMI_BOLD_ITALIC("SemiBoldItalic", 17);

    private String code;
    private Integer position;

    FontStyle(String code, Integer position) {

        this.code = code;
        this.position = position;

    }

    public Integer getPosition() {

        return position;

    }

    public String getCode() {

        return code;

    }

    public static FontStyle getByCode(String code) {

        if (code == null)   return UNKNOWN;

        for (FontStyle fontStyle : values()) {

            if (fontStyle.getCode().equals(code)) {

                return fontStyle;

            }

        }

        return UNKNOWN;

    }

    public static FontStyle getByPosition(Integer position) {

        if (position == null)   return UNKNOWN;

        for (FontStyle fontStyle : values()) {

            if (fontStyle.getPosition().equals(position)) {

                return fontStyle;

            }

        }

        return UNKNOWN;

    }

}
