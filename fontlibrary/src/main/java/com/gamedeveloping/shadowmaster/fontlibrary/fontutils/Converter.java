package com.gamedeveloping.shadowmaster.fontlibrary.fontutils;

/**
 * Created by ShadowMaster on 29.07.2017.
 */

interface Converter <From, To>{

    To forward(From from);
    From backward(To to);

}
