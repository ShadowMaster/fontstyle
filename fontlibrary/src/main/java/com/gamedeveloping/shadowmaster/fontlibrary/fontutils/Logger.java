package com.gamedeveloping.shadowmaster.fontlibrary.fontutils;

import android.util.Log;

/**
 * Created by ShadowMaster on 29.07.2017.
 */

class Logger {

    private static boolean enableLogging = true;

    static void enable() {

        enableLogging = true;

    }

    static void disable() {

        enableLogging = false;

    }

    static void v(String tag, String string) {

        if (enableLogging) {

            Log.v(tag, string);

        }

    }

    static void d(String tag, String string) {

        if (enableLogging) {

            Log.d(tag, string);

        }

    }

    static void w(String tag, String string) {

        if (enableLogging) {

            Log.w(tag, string);

        }

    }

    static void e(String tag, String string) {

        if (enableLogging) {

            Log.e(tag, string);

        }

    }

    static void i(String tag, String string) {

        if (enableLogging) {

            Log.i(tag, string);

        }

    }

}
